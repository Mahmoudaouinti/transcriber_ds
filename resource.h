//{{NO_DEPENDENCIES}}
// fichier Include Microsoft Visual C++.
// Utilis� par Transcriber.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TRANSCRIBER_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDC_EDIT1                       1000
#define ID_WAV_S                        1000
#define IDC_BUTTON1                     1001
#define ID_BROWSE_WAV                   1001
#define IDC_EDIT2                       1002
#define ID_OUTPUT                       1002
#define IDC_BUTTON2                     1003
#define ID_TRANSCRIBE                   1003
#define IDC_EDIT3                       1004
#define ID_MODEL_S                      1004
#define IDC_BUTTON3                     1005
#define ID_BROWSE_MODEL                 1005
#define IDC_BUTTON4                     1007
#define ID_LOAD                         1007
#define ID_CLEAR                        1008
#define ID_Loader                       1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
