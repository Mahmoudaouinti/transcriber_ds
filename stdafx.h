#pragma once
#define _USE_MATH_DEFINES
#include"pch.h"
#ifndef ADDITIONAL_INCLUDES
#define ADDITIONAL_INCLUDES
#include <atomic>
#include <cassert>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>
#include <string>
#include <memory>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sstream>
#include <iostream> 
#include <crtdbg.h>
#include <ctime>
#include <chrono>
//#include "Mmsystem.h"

#endif


#ifndef NOMINMAX 
#define NOMINMAX
#endif // !NOMINMAX 
#include <algorithm>
using namespace std::chrono;
namespace Gdiplus
{
	using std::min;
	using std::max;

}


#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#include <afxdisp.h>        // MFC Automation classes



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC support for ribbons and control bars




#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif







#define INBUF_SIZE 4096
#define AUDIO_INBUF_SIZE 20480
#define AUDIO_REFILL_THRESH 4096
#ifndef TRANSCRIBE_LIB
#define TRANSCRIBE_LIB
//*******FFMPEG LIB******//
#include "include/deepspeech.h"
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswresample/swresample.h>




#endif
#include "include/AudioFile.h"

#ifndef DEEP_SPEECH
#define DEEP_SPEECH
#include "STT.h"
#endif // !DEEP_SPEECH



#include <afxcontrolbars.h>
