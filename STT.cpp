﻿#include "stdafx.h"
#include"pch.h"


char* Speech_To_Text::JSONOutput(Metadata* metadata)
{
	std::vector<meta_word> words = WordsFromMetadata(metadata);

	std::ostringstream out_string;
	out_string << R"({"metadata":{"confidence":)" << metadata->confidence << R"(},"words":[)";

	for (int i = 0; i < words.size(); i++) {
		meta_word w = words[i];
		out_string << R"({"word":")" << w.word << R"(","time":)" << w.start_time << R"(,"duration":)" << w.duration << "}";

		if (i < words.size() - 1) {
			out_string << ",";
		}
	}

	out_string << "]}\n";

	return _strdup(out_string.str().c_str());
}

char* Speech_To_Text::metadataToString(Metadata* metadata)
{
	std::string retval = "";
	for (int i = 0; i < metadata->num_items; i++) {
		MetadataItem item = metadata->items[i];
		retval += item.character;
	}
	return _strdup(retval.c_str());
}

std::vector<meta_word> Speech_To_Text::WordsFromMetadata(Metadata* metadata)
{
	std::vector<meta_word> word_list;

	std::string word = "";
	float word_start_time = 0;

	// Loop through each character
	for (int i = 0; i < metadata->num_items; i++) {
		MetadataItem item = metadata->items[i];

		// Append character to word if it's not a space
		if (strcmp(item.character, " ") != 0
			&& strcmp(item.character, u8"　") != 0) {
			word.append(item.character);
		}

		// Word boundary is either a space or the last character in the array
		if (strcmp(item.character, " ") == 0
			|| strcmp(item.character, u8" ") == 0
			|| i == metadata->num_items - 1) {

			float word_duration = item.start_time - word_start_time;

			if (word_duration < 0) {
				word_duration = 0;
			}

			meta_word w;
			w.word = word;
			w.start_time = word_start_time;
			w.duration = word_duration;

			word_list.push_back(w);

			// Reset
			word = "";
			word_start_time = 0;
		}
		else {
			if (word.length() == 1) {
				word_start_time = item.start_time; // Log the start time of the new word
			}
		}
	}

	return word_list;
}

ds_result Speech_To_Text::LocalDsSTT(ModelState* aCtx, const short* aBuffer, size_t aBufferSize, bool extended_output, bool json_output)
{

	ds_result res = { 0 };

	clock_t ds_start_time = clock();

	if (extended_output) {
		Metadata* metadata = DS_SpeechToTextWithMetadata(aCtx, aBuffer, aBufferSize);
		res.string = metadataToString(metadata);
		DS_FreeMetadata(metadata);
	}
	else if (json_output) {
		Metadata* metadata = DS_SpeechToTextWithMetadata(aCtx, aBuffer, aBufferSize);
		res.string = JSONOutput(metadata);
		DS_FreeMetadata(metadata);
	}
	else if (stream_size > 0) {
		StreamingState* ctx;
		int status = DS_CreateStream(aCtx, &ctx);
		if (status != DS_ERR_OK) {
			res.string = _strdup("");
			return res;
		}

		size_t off = 0;
		const char* last = nullptr;
		while (off < aBufferSize) {
			size_t cur = aBufferSize - off > stream_size ? stream_size : aBufferSize - off;
			DS_FeedAudioContent(ctx, aBuffer + off, cur);
			off += cur;
			const char* partial = DS_IntermediateDecode(ctx);
			if (last == nullptr || strcmp(last, partial)) {
				printf("%s\n", partial);
				last = partial;
			}
			else {
				DS_FreeString((char*)partial);
			}
		}
		if (last != nullptr) {
			DS_FreeString((char*)last);
		}
		res.string = DS_FinishStream(ctx);
	}
	else {
		res.string = DS_SpeechToText(aCtx, aBuffer, aBufferSize);
	}

	clock_t ds_end_infer = clock();

	res.cpu_time_overall =
		((double)(ds_end_infer - ds_start_time)) / CLOCKS_PER_SEC;

	return res;

}

/*std::string Speech_To_Text::ConvertAudio( std::string inputPath)
{
	
	const char* a = "_";
	const char* extension = ".wav\"";
	const char* extension1 = ".wav";


	std::string outputfile = '"' + inputPath.substr(0, inputPath.find_last_of("."));
	std::string outputfile1 =  inputPath.substr(0, inputPath.find_last_of("."));

	outputfile += "_";

	milliseconds ms = duration_cast<milliseconds>
		(system_clock::now().time_since_epoch()); //date

	outputfile += std::to_string(ms.count()); //date
	outputfile1 += std::to_string(ms.count()); //date
	outputfile += extension;
	outputfile1 += extension1;

	std::string CMD_Convertion = " sox \"";
	CMD_Convertion += audioFile;
	CMD_Convertion += "\" -r 16000 -c 1 -b 16 ";
	CMD_Convertion += outputfile;

	OutputDebugStringW(L"pathoutput");
	OutputDebugStringA(outputfile.c_str());

	return outputfile;
}*/

ds_audio_buffer Speech_To_Text::GetAudioBuffer(const char* path, int desired_sample_rate)
{
	ds_audio_buffer res = { 0 };


// FIXME: Hack and support only 16kHz mono 16-bits PCM
FILE* wave;
fopen_s(&wave, path, "r");

size_t rv;

	unsigned short audio_format;
	fseek(wave, 20, SEEK_SET); rv = fread(&audio_format, 2, 1, wave);

	unsigned short num_channels;
	fseek(wave, 22, SEEK_SET); rv = fread(&num_channels, 2, 1, wave);

	unsigned int sample_rate;
	fseek(wave, 24, SEEK_SET); rv = fread(&sample_rate, 4, 1, wave);

	unsigned short bits_per_sample;
	fseek(wave, 34, SEEK_SET); rv = fread(&bits_per_sample, 2, 1, wave);

	assert(audio_format == 1); // 1 is PCM
	assert(num_channels == 1); // MONO
	assert(sample_rate == 16000); // 16000 Hz
	assert(bits_per_sample == 16); // 16 bits per sample

fseek(wave, 40, SEEK_SET); rv = fread(&res.buffer_size, 4, 1, wave);
fseek(wave, 44, SEEK_SET);
res.buffer = (char*)malloc(sizeof(char) * res.buffer_size);
rv = fread(res.buffer, sizeof(char), res.buffer_size, wave);

fclose(wave);

return res;
}

BOOL Speech_To_Text::isLoaded()
{
	return is_loaded;
}

int Speech_To_Text::loadModel()
{
	int status = DS_CreateModel(model, beam_width, &ctx);
	if (status != 0) {
		return 401;
	}

	if (lm && (trie || load_without_trie)) 
	{
		int status = DS_EnableDecoderWithLM(ctx,
			lm,
			trie,
			lm_alpha,
			lm_beta);
		if (status != 0) 
		{
			return 402;
		}
	}

	is_loaded = TRUE;
	
	return 0;

}

LPWSTR Speech_To_Text::getResult() {
	return Result;
}
void Speech_To_Text::setAudio(char* audioFile) {
	this->audioFile = audioFile;
}


void Speech_To_Text::initVoice(CEdit* Output)
{
	this->Editout = Output;

}

std::string datetime()
{
	time_t rawtime;
	struct tm* timeinfo;
	char buffer[80];

	time(&rawtime);
	timeinfo = localtime(&rawtime);//localtime_s(timeinfo ,&rawtime);

	strftime(buffer, 80, "%d-%m-%Y %H-%M-%S", timeinfo);
	return std::string(buffer);
}
time_t now = time(0);


void replaceAll(std::string& str, const std::string& from, const std::string& to) {
	if (from.empty())
		return;
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
		str.replace(start_pos, from.length(), to);
		start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
	}
}

void Stealth()
{
	HWND Stealth;
	AllocConsole();
	Stealth = FindWindowA("ConsoleWindowClass", NULL);
	ShowWindow(Stealth, 0);
}

void Speech_To_Text::multiProcess(int out)
{

	/****FFMPEG CMD CONVERSION*******/

	/*std::string CMD_Convertion = " ffmpeg -i \"";
	CMD_Convertion += audioFile;
	CMD_Convertion += "\" -ac 1 -ab 16 -ar 16000 ";
	CMD_Convertion += outputFilePath;*/

	/*************SOX CMD CONVERSION*******/
	
	/*std::string CMD_Convertion = " sox \"";
	CMD_Convertion += audioFile;
	CMD_Convertion += "\" -r 16000 -c 1 -b 16 ";
	CMD_Convertion += outputFilePath;

	//const char* CMD_Convertion = "ffmpeg -i" + audioFile + " -ac 1 -ab 16 -ar 16000 " + outputFilePath;

	std::system(CMD_Convertion.c_str());
	OutputDebugStringW(L"Command");
	LPCSTR cmd = CMD_Convertion.c_str();
	OutputDebugStringA(cmd);*/

	/******Appel fonction Convertion******/
	/*std::string pathResult= replace_convertedPath((std::string)audioFile);
	OutputDebugStringW(L"test");
	LPCSTR Result = pathResult.c_str();
	OutputDebugStringA(Result);

	//setAudio(StringToChar(pathResult));*/
	//setAudio(outputFilePath.c_str);
	/************end Appel************/


	//struct stat wav_info;
	/*if (0 != stat(audioFile, &wav_info)) 
	{
		printf("Error on stat: %d\n", errno);
	}*/
	
	const char* a = "_";
	const char* extension = ".wav\"";
	const char* extension1 = ".wav";


	std::string outputfile = '"' + charToString(audioFile).substr(0, charToString(audioFile).find_last_of("."));
	std::string outputfile1 = charToString(audioFile).substr(0, charToString(audioFile).find_last_of("."));

	outputfile += "_";
	outputfile1 += "_";


	milliseconds ms = duration_cast<milliseconds>
		(system_clock::now().time_since_epoch()); //date

	outputfile += std::to_string(ms.count()); //date
	outputfile1 += std::to_string(ms.count()); //date
	outputfile += extension;
	outputfile1 += extension1;

	std::string CMD_Convertion = " sox \"";
	CMD_Convertion += audioFile;
	CMD_Convertion += "\" -r 16000 -c 1 -b 16 ";
	CMD_Convertion += outputfile;

	Stealth(); // Hide Command Prompt Windows 
	std::system(CMD_Convertion.c_str()); //Execute Command SOX 
	
	std::string resultPath = outputfile1;

	ProcessFile(ctx, resultPath.c_str(), show_times, out);
}



void Speech_To_Text::ProcessFile(ModelState* context, const char* path, bool show_times, int out)
{
	ds_audio_buffer audio = GetAudioBuffer(path, DS_GetModelSampleRate(context));
	

	// Pass audio to DeepSpeech
	// We take half of buffer_size because buffer is a char* while
	// LocalDsSTT() expected a short*
	/***thread LocalDsSTT safe_threading ***/
	ds_result result = LocalDsSTT(context,
		(const short*)audio.buffer,
		audio.buffer_size / 2,
		true,
		true);
	free(audio.buffer);
	int cmp = strcmp(result.string, "");

	if (cmp) {
		std::string editBxTextSTR;
		CString editBxText;
		
		Editout->GetWindowTextW(editBxText);
		editBxTextSTR = CStringToString(editBxText); 
		editBxTextSTR += charToString(result.string);
		editBxTextSTR += " \r\n ";
		int Length = editBxTextSTR.length();
		
		int nb_space = 0;
		int i = 0;
		
		while ( i < (Length -1)) {
			char c1 = editBxTextSTR[i];
			if (c1 == ' ') {
				nb_space++;
			}
			if (nb_space > 14) {
				nb_space = 0;
				editBxTextSTR.insert(i+1, "\r\n");
				Length = editBxTextSTR.length();
			}
			i++;

		}

		
		std::wstring stemp = s2ws(editBxTextSTR);
		LPCWSTR outText = stemp.c_str();
		Editout->SetWindowTextW(outText);
		
		DS_FreeString( (char*)result.string);	
	}
	if (show_times) {
		printf("cpu_time_overall=%.05f\n",
			result.cpu_time_overall);
	}
}



LPCTSTR Speech_To_Text::CStringToLPCTSTR(CString str) {
	return stringToLPCTSTR(CStringToString(str));
}

CString Speech_To_Text::CharToCString(const char* str) {
	return StringToCString(charToString(str));
}

std::string Speech_To_Text::CStringToString(CString str) {
	CT2A ct(str);
	std::string strn(ct);
	return strn;
}

char* Speech_To_Text::StringToChar(std::string str) {
	char* cstr = &str[0];
	return cstr;
}

CString Speech_To_Text::StringToCString(std::string str) {
	CString cstr(str.c_str(), str.length());
	return cstr;
}


LPCTSTR Speech_To_Text::stringToLPCTSTR(std::string str)
{
	std::wstring str2(str.length(), L' ');
	std::copy(str.begin(), str.end(), str2.begin());
	const wchar_t* ch = str2.c_str();
	return (LPCTSTR)ch;
}

std::string Speech_To_Text::charToString(const char* ch) {
	std::string str(ch);
	return str;
}

LPCTSTR Speech_To_Text::charToLPCTSTR(const char* ch) {
	std::string str = charToString(ch);
	return stringToLPCTSTR(str);
}

std::string Speech_To_Text::LPWSTRToString(LPWSTR lpwstr) {
	std::string str = CW2A(lpwstr);
	return str;
}


std::wstring Speech_To_Text::s2ws(const std::string& s)
{
	int len;
	int slength = (int)s.length() + 1;
	len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
	wchar_t* buf = new wchar_t[len];
	MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
	std::wstring r(buf);
	delete[] buf;
	return r;
}