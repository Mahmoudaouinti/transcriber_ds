
// TranscriberDlg.cpp : fichier d'implémentation
//

#include "pch.h"
#include "stdafx.h"
#include "Transcriber.h"
#include "TranscriberDlg.h"
#include "afxdialogex.h"



using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CTranscriberDlg::CTranscriberDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TRANSCRIBER_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTranscriberDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTranscriberDlg, CDialogEx)

	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()

	ON_BN_CLICKED(IDCANCEL, &CTranscriberDlg::OnBnClickedCancel)
	
	
	ON_BN_CLICKED(ID_BROWSE_WAV, &CTranscriberDlg::OnBnClickedBrowseWav)
	ON_BN_CLICKED(ID_TRANSCRIBE, &CTranscriberDlg::OnBnClickedTranscribe)
	ON_BN_CLICKED(ID_CLEAR, &CTranscriberDlg::OnBnClickedClear)
END_MESSAGE_MAP()


// gestionnaires de messages de CTranscriberDlg

BOOL CTranscriberDlg::OnInitDialog()
{
	WAV_PATH = (CEdit*)GetDlgItem(ID_WAV_S);
	EditOutput = (CEdit*)GetDlgItem(ID_OUTPUT);
	TRANSCRIBE = (CButton*)GetDlgItem(ID_TRANSCRIBE);
	//WAV_PATH->SetWindowTextW(L"Select WAV File ...");
	stt = new Speech_To_Text();

	stt->loadModel();
	if (stt->isLoaded())
		MessageBox(L"Model Loaded successfuly");
	else
		MessageBox(L"Error");

	stt->initVoice(EditOutput);


	// IDM_ABOUTBOX doit se trouver dans la plage des commandes système.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Définir l'icône de cette boîte de dialogue.  L'infrastructure effectue cela automatiquement
	//  lorsque la fenêtre principale de l'application n'est pas une boîte de dialogue
	SetIcon(m_hIcon, TRUE);			// Définir une grande icône
	SetIcon(m_hIcon, FALSE);		// Définir une petite icône

	// TODO: ajoutez ici une initialisation supplémentaire

	return TRUE;  // retourne TRUE, sauf si vous avez défini le focus sur un contrôle
}



// Si vous ajoutez un bouton Réduire à votre boîte de dialogue, vous devez utiliser le code ci-dessous
//  pour dessiner l'icône.  Pour les applications MFC utilisant le modèle Document/Vue,
//  cela est fait automatiquement par l'infrastructure.

void CTranscriberDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // contexte de périphérique pour la peinture

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrer l'icône dans le rectangle client
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dessiner l'icône
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Le système appelle cette fonction pour obtenir le curseur à afficher lorsque l'utilisateur fait glisser
//  la fenêtre réduite.
HCURSOR CTranscriberDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}
//---------------------------------------------------------------------------------------------------------
//CTranscriberDlg Dialog


void CTranscriberDlg::OnBnClickedCancel()
{
	// TODO: ajoutez ici le code de votre gestionnaire de notification de contrôle
	CDialogEx::OnCancel();
}

void CTranscriberDlg::OnBnClickedBrowseWav()
{
	CFileDialog fileDlg(TRUE, L"WAV", L".wav", OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST| OFN_HIDEREADONLY, L"WAV File (*.WAV)|*.WAV||", this);
	int iRet = fileDlg.DoModal();
	str_Pathname = fileDlg.GetPathName();
	UpdateData(FALSE);
	WAV_PATH->SetWindowTextW(str_Pathname);
	if (iRet == IDOK)
	{
		try
		{
			CStdioFile file((str_Pathname), CFile::modeRead);
			CString str, contentstr = _T("");
			while (file.ReadString(str))
			{
				contentstr += str;
				contentstr += _T("\n \n");
			}
		}
		catch (std::exception& e)
		{
			MessageBox(L"Error");

		}
	}
}

void CTranscriberDlg::OnBnClickedTranscribe()
{
	
	CString windowTxt;
	WAV_PATH->GetWindowTextW(windowTxt);
	std::string strp = stt->CStringToString(windowTxt);
	char* char_path;
	char_path = stt->StringToChar(strp);
	stt->setAudio(char_path);
	

	//stt->multiProcess(0);
	//TRANSCRIBE->EnableWindow(0);
	services.push_back(std::thread(&Speech_To_Text::multiProcess, stt, 0)); //Safe Thread Transcription
	//TRANSCRIBE->EnableWindow(1);
	
}


void CTranscriberDlg::OnBnClickedClear()
{
	
	EditOutput->SetWindowTextW(L"");
		
}

CTranscriberDlg::~CTranscriberDlg()
{
	for (auto& th : services) {
		th.join();
	}
}
