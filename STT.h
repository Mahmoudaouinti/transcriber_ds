#pragma once
#include "stdafx.h"

#ifndef  STT_NEOP
#define STT_NEOP

typedef struct {
	const char* string;
	double cpu_time_overall;
} ds_result;

struct meta_word {
	std::string word;
	float start_time;
	float duration;
};

typedef struct {
	char* buffer;
	size_t buffer_size;
} ds_audio_buffer;

class Speech_To_Text {

private:
	float seconds_offset = 0.0f;
	int play_cursor = 0;
	const char* soundFile = { "audio.wav" };
	UINT fuSound;
	BOOL is_loaded = FALSE;

	//----------DeepSpeech-------------------------------
	char* model = "models/output_graph.pbmm";
	char* lm = "models/lm.binary";
	char* trie = "models/trie";
	char* audioFile = "audio.wav";
	std::string outputfile;
	ModelState* ctx;
	wchar_t* Result;
	CEdit* Editout;
	//-------------------------------------------
	bool load_without_trie = false;

	bool show_times = false;

	bool has_versions = false;

	bool extended_metadata = false;

	bool json_output = false;

	int stream_size = 0;

	//----DS Parameters---------------

	int beam_width = 500;

	float lm_alpha = 0.75f;

	float lm_beta = 1.85f;

public: 
	BOOL isLoaded();
	int loadModel();
	void initVoice(CEdit* Output);
	ds_audio_buffer GetAudioBuffer(const char* path, int desired_sample_rate);
	void multiProcess(int out);
	void ProcessFile(ModelState* context, const char* path, bool show_times, int out);
	ds_result LocalDsSTT(ModelState* aCtx, const short* aBuffer, size_t aBufferSize, bool extended_output, bool json_output);
	char* JSONOutput(Metadata* metadata);
	std::vector<meta_word> WordsFromMetadata(Metadata* metadata);
	char* metadataToString(Metadata* metadata);
	LPWSTR getResult();
	void setAudio(char* audioFile);
	//std::string ConvertAudio( std::string inputpath);
	 


	//--------------Convert TypeData-----------------------------------

	static char* StringToChar(std::string str);
	static LPCTSTR stringToLPCTSTR(std::string str);
	static LPCTSTR CStringToLPCTSTR(CString str);
	static std::string CStringToString(CString str);
	static CString StringToCString(std::string str);
	static std::string charToString(const char* str);
	static CString CharToCString(const char* str);
	static LPCTSTR charToLPCTSTR(const char* str);
	static std::string LPWSTRToString(LPWSTR lpwstr);

	std::wstring s2ws(const std::string& s);

};


#endif // ! STT_NEOP
