#pragma once
#include "stdafx.h"

class SttThread
{
public:
	void set_STT(Speech_To_Text* stt);
	void run();
	void Process();
	void Start();
	//void OnStart();
	void Stop();
	BOOL isRunning();
	void setAudio(char* audioFile);
	
	~SttThread();

private:
	
	
	std::string ch;
	std::thread* t1;
	std::vector<std::thread> services;
	std::mutex lock;
	Speech_To_Text* stt;
	BOOL start = FALSE;
	//BOOL onStart = FALSE;

};