
// TranscriberDlg.h : fichier d'en-tête
//

#pragma once
#pragma warning

// boîte de dialogue de CTranscriberDlg
class CTranscriberDlg : public CDialogEx
{
// Construction
public:
	
	CTranscriberDlg(CWnd* pParent = nullptr);	// constructeur standard

// Données de boîte de dialogue
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TRANSCRIBER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Prise en charge de DDX/DDV


// Implémentation
protected:
	HICON m_hIcon;

	// Fonctions générées de la table des messages
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	
	

private:
	Speech_To_Text* stt;
	CEdit* EditOutput;
	CEdit* WAV_PATH;
	CButton* TRANSCRIBE;
	CButton* Clear;
	CString str_Pathname;
	std::vector<std::thread> services;
	std::thread t1;
	


public:
	
	~CTranscriberDlg();
	afx_msg void OnBnClickedBrowseWav();
	afx_msg void OnBnClickedTranscribe();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedClear();
};
